//
//  SecondViewController.swift
//  To Do
//
//  Created by Ahmed adham on 2/19/18.
//  Copyright © 2018 Ahmed adham. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController , UITextFieldDelegate{

    @IBOutlet weak var textfield: UITextField!
    
    var items : [String] = []
   
    @IBAction func addPressed(_ sender: UIButton) {
        
        let itemObj = UserDefaults.standard.object(forKey: "items")
        
        if let temp = itemObj as? [String] {
        
            items = temp
            items.append(textfield.text!)
        
        }
        else {
            items.append(textfield.text!)
        }
        
        UserDefaults.standard.set(items, forKey: "items")
        
        textfield.text = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textfield.resignFirstResponder()
        return true
    }
}

