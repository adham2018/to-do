//
//  FirstViewController.swift
//  To Do
//
//  Created by Ahmed adham on 2/19/18.
//  Copyright © 2018 Ahmed adham. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController , UITableViewDataSource , UITableViewDelegate{

    
    @IBOutlet weak var tableView: UITableView!
    
    var items : [String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.reloadData()
        
        }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
        cell.textLabel?.text = items[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    

    override func viewDidAppear(_ animated: Bool) {
        
        let itemObj = UserDefaults.standard.object(forKey: "items")
        if let temp = itemObj as? [String] {
        
            items = temp
            
        }
        
        tableView.reloadData()
    }


    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {

        if editingStyle == UITableViewCellEditingStyle.delete {
            items.remove(at: indexPath.row)
            tableView.reloadData()
            UserDefaults.standard.set(items, forKey: "items")
        }
    }

}

